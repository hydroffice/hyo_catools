HydrOffice CA Tools
===================

.. image:: https://img.shields.io/pypi/v/hyo.ca.svg
    :target: https://pypi.python.org/pypi/hyo.ca
    :alt: PyPi version

.. image:: https://img.shields.io/badge/docs-latest-brightgreen.svg
    :target: https://www.hydroffice.org/manuals/catools/index.html
    :alt: Latest Documentation

.. image:: https://ci.appveyor.com/api/projects/status/bwlc3h00jyl2upqw?svg=true
    :target: https://ci.appveyor.com/project/appveyor1/hyo-ca
    :alt: AppVeyor Status

.. image:: https://travis-ci.org/hydroffice/hyo_ca.svg?branch=master
    :target: https://travis-ci.com/hydroffice/hyo_ca
    :alt: Travis-CI Status

.. image:: https://api.codacy.com/project/badge/Grade/3b37f9141ece4339b3d35bdff48865c3
    :target: https://www.codacy.com/app/hydroffice/hyo_ca/dashboard
    :alt: Codacy badge


* Code: `GitHub repo <https://github.com/hydroffice/hyo_ca>`_
* Project page: `url <https://www.hydroffice.org/qctools/main>`_
* Download page: `url <https://bitbucket.org/hydroffice/hyo_catools/downloads/>`_
* License: LGPLv3 license (See `LICENSE <https://github.com/hydroffice/hyo_ca/raw/master/LICENSE>`_)

|

General info
------------

.. image:: https://github.com/hydroffice/hyo_ca/raw/master/hyo/ca/catools/media/favicon.png
    :alt: logo

HydrOffice is a research development environment for ocean mapping. Its aim is to provide a collection of
hydro-packages to deal with specific issues in such a field, speeding up both algorithms testing and
research-2-operation.

This package provides functionalities to verify the adequacy of nautical charts.